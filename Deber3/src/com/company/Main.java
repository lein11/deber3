package com.company;

public class Main {

    public static void main(String[] args) {

        //Ejemplos Operadores Unarios
        int a = 5;
        int b = 4;

        int c = ++a;
        int d = b++;
        int e = ~a;
        int f = -b;

        System.out.println("Ejemplos Operadores Unarios");
        System.out.println("Ejemplo ++num: " + c);
        System.out.println("Ejemplo num++: " + d);
        System.out.println("Ejemplo ~: " + e);
        System.out.println("Ejemplo -: " + f);
        System.out.println();

        //Ejemplos Binarios a=6 , b=5 debido al ++

        int g = a+b;
        int h = a-b;
        int m = a*b;
        int n = a/b;
        //Se hace doble porque en int lo mas probable es que se aproxime a 0
        double aa = a;
        double bb = b;
        double res1 = aa%2.0;
        double res2 = bb%2.0;

        System.out.println("Ejemplos Operadores Binarios");
        System.out.println("Ejemplo suma: " + g);
        System.out.println("Ejemplo resta: " + h);
        System.out.println("Ejemplo multiplicación: " + m);
        System.out.println("Ejemplo División: " + n);
        System.out.println("Ejemplo Residuo 1 de a para 2: " + res1);
        System.out.println("Ejemplo Residuo 2 de b para 2: " + res2);
        System.out.println();

        //Ejemplos Comparadores Binarios.
        System.out.println("Ejemplos Comparadores Binarios a=6 , b=5");
        if(a>b){
            System.out.println("a es mayor a b");
        }

        if(b<a){
            System.out.println("b es menor a a");
        }

        if((a*b)==(b*a)){
            System.out.println("Funciona la igualdad");
        }

        if(a!=b){
            System.out.println("Numeros no Son iguales");
        }
        System.out.println();

        //Ejemplos Comparadores Booleanos.
        System.out.println("Ejemplos Comparadores Booleanos");

        if(a>=0 && b<=10){
            System.out.println("Ejemplo && si las 2 condiciones son correctas es true");
        }

        if(a>=0 && b<=2){
            System.out.println("Ejemplo &&(and) si las 2 condiciones son correctas es true");
        }
        else
            System.out.println("Caso contrario es false");

        if(a>0 || b<10)
            System.out.println("Ejemplo ||(or) si 1 condicion es correcta es true");

        if(a<0 || b<10)
            System.out.println("Ejemplo ||(or) si 1 condicion es correcta es true");

        if(a<0 || b<2)
            System.out.println("Ejemplo ||(or) si 1 condicion es correcta es true");
        else
            System.out.println("Si las 2 son falsas entonces es false");

        if(a>0 ^ b<2)
            System.out.println("Ejemplo ^(Exclusice or) si una condicion es verdadera y la otra falsa  es true");

        if(a>0 ^ b<10)
            System.out.println("Ejemplo ^(Exclusice or) si una condicion es verdadera y la otra falsa  es true");
        else
            System.out.println("Ejemplo ^(Exclusice or) si ambas condiciones son verdaderas o falsa  es false");

        System.out.println();

        //Ejemplos Conversion por asignación primitivoa
        System.out.println("Ejemplos Conversion por asignación primitivoa");
        byte bbb = 6;
        short aaa = 5;

        System.out.println("Short y Byte son los primitivos con menor memoria por lo que pueden ser convertidos a cualquiera de rango mayor");

        int ab = aaa;
        int ba = bbb;
        long aba = aaa;
        long bab = bbb;
        float fa = aaa;
        float fb = bbb;
        double da = aaa;
        double db = bbb;

        System.out.println("COnversion a int: " + ab + " " + ba);
        System.out.println("COnversion a long: " + aba + " " + bab);
        System.out.println("COnversion a float: " + fa + " " + fb);
        System.out.println("COnversion a double: " + da + " " + db);
        System.out.println();

        //Ejemplos Conversion por aritmetica primitivoa
        System.out.println("Ejemplos Conversion por aritmetica primitivoa");
        double dda = da + aaa;
        float ffa = fa + aaa;
        long la = aba + aaa;

        System.out.println("Se suman valores de rango mayor con menor, la respuesta debe dar el rango mayor");
        System.out.println("COnversion a double: " + dda);
        System.out.println("COnversion a float: " + ffa);
        System.out.println("COnversion a long: " + la);
        System.out.println();

        //Ejemplos Conversion por metodos primitivoa
        System.out.println("Ejemplos Conversion por aritmetica primitivoa");
        System.out.println("Un metodo que pida de dato un primitivo de rango mayor acepta variables de rango inferior");
        int entradaMenor = 5;
        System.out.println("Ejemplo Converiosn por metodo: " + ejemMetodo(entradaMenor));
        System.out.println();

        //Ejemplos Casting primitivoa
        System.out.println("Se puede hacer conversion de un primitivo de rango mayor a menor perdiendo datos");
        double dd1 = 30.3;
        double dd2 = 5.5;
        double dd3 = 4.9;

        int i1 = (int) dd1;
        short s1 = (short) dd2;
        long l1 = (long) dd3;

        System.out.println("COnversion de double a int: " + i1);
        System.out.println("COnversion de double a short: " + s1);
        System.out.println("COnversion de double a long: " + l1);

        //Ejemplos Conversion por asignación NO primitivoa
        interfaceEjemplo e1;
        claseEjemplo c1 = e1;

    }

    public static double ejemMetodo(double entrada){
        return entrada * entrada;
    }


}
